package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import models.Situation;

public class SituationDAO extends ConnectionDAO{

	public ArrayList<Situation> all() {
		
		ArrayList<Situation> list = new ArrayList<>();		
		openDB();
		try {
			String sql = "SELECT * FROM `situations`";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				Situation current = new Situation();
				current.setId(rs.getInt("id"));
				current.setName(rs.getString("name"));
				list.add(current);
			}
		} catch (Exception e) {
			throw new RuntimeException("SQL error, -situation- " + e.getMessage());
		} finally {
			closeDB();
		}
		return list;
	}
}
