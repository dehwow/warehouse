package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import models.Sector;


public class SectorDAO extends ConnectionDAO{
	
public ArrayList<Sector> all() {
		
		ArrayList<Sector> list = new ArrayList<>();		
		openDB();
		try {
			String sql = "SELECT * FROM `sectors`";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				Sector current = new Sector();
				current.setId(rs.getInt("id"));
				current.setName(rs.getString("name"));
				list.add(current);
			}
		} catch (Exception e) {
			throw new RuntimeException("SQL error, -sector- " + e.getMessage());
		} finally {
			closeDB();
		}
		return list;
	}

}
