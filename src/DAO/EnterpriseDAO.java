package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import models.Enterprise;

public class EnterpriseDAO extends ConnectionDAO {
	
	public ArrayList<Enterprise> all() {

		ArrayList<Enterprise> list = new ArrayList<>();
		openDB();
		try {
			String sql = "SELECT * FROM `enterprises`";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				Enterprise current = new Enterprise();
				current.setId(rs.getInt("id"));
				current.setName(rs.getString("name"));
				list.add(current);
			}
		} catch (Exception e) {
			throw new RuntimeException("SQL error, -Enterprise- " + e.getMessage());
		} finally {
			closeDB();
		}
		return list;
	}
}
