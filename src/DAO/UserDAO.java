package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import models.User;

public class UserDAO extends ConnectionDAO {

	public User findUser(String name) {
		User currentUser = null;
		openDB();
		try {
			String sql = "SELECT * FROM `users` WHERE employee_id = (SELECT id FROM employees WHERE id like ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				currentUser = new User();
				currentUser.setId(rs.getInt("id"));
				currentUser.setEmplooyeeId(rs.getInt("employee_id"));
				currentUser.setPassword(rs.getString("password"));
				currentUser.setSalt(rs.getString("salt"));
				currentUser.setPermission(rs.getInt("permission"));
			}
		} catch (Exception e) {
			throw new RuntimeException("Erro. Usuario incorreto: " + e.getMessage());
		} finally {
			closeDB();
		}
		return currentUser;
	}

}
