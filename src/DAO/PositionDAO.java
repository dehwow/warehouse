package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import models.Position;

public class PositionDAO extends ConnectionDAO {

	public ArrayList<Position> all() {

		ArrayList<Position> list = new ArrayList<>();
		openDB();
		try {
			String sql = "SELECT * FROM `positions`";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				Position current = new Position();
				current.setId(rs.getInt("id"));
				current.setName(rs.getString("name"));
				current.setSalaryBase(rs.getDouble("salary_base"));
				list.add(current);
			}
		} catch (Exception e) {
			throw new RuntimeException("SQL error, -position- " + e.getMessage());
		} finally {
			closeDB();
		}
		return list;
	}
}
