package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.text.MaskFormatter;

import com.jgoodies.validation.Validator;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.toedter.calendar.JCalendar;

import controlloers.EmployeeController;
import controlloers.MainController;
import helpers.ComboObject;
import helpers.ViewHelpers;
import models.Employee;
import models.Enterprise;
import models.Position;
import models.Sector;
import models.Situation;

public class EmployeeNewView extends JPanel implements VisualWindow {

	/**
	 * Ideal min size frame = 650 x 550 not responsible
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lbName;
	private JLabel lbAddress;
	private JLabel lbDistrict;
	private JLabel lbTell;
	private JLabel lbAdminssionDate;
	private JLabel lbId;
	private JLabel lbPosition;
	private JLabel lbSector;
	private JLabel lbEnterprise;
	private JLabel lbSituation;
	private JLabel lbPassword;
	private JLabel lbConfirmPass;
	private JLabel lbComplement;
	private JLabel lbBirth;
	private JTextField tfId;
	private JTextField tfName;
	private JTextField tfAddress;
	private JTextField tfDistrict;
	private JTextField tfComplement;
	private MaskFormatter date;
	private MaskFormatter tell;
	private JFormattedTextField tfTell;
	private JFormattedTextField tfBirth;
	private JFormattedTextField tfAdmissionDate;
	private JCalendar jcDate;
	private JCalendar jcDate2;
	private JComboBox<Position> jcPosition;
	private JComboBox<Sector> jcSector;
	private JComboBox<Enterprise> jcEnterprise;
	private JComboBox<Situation> jcSituation;
	private JPasswordField pfPassword;
	private JPasswordField pfConfirmPass;
	private JCheckBox cbEnable;
	private JButton btRegister;
	private JButton btCancel;
	private JButton btCalendar;
	private JButton btCalendar2;
	private JButton btcOk;
	private JButton btcOk2;
	private JButton btcCancel;
	private JFrame parent;
	private JFrame JCFrame;
	private Locale locale = new Locale("pt", "BR");
	private JPanel btnPanel;
	private ImageIcon iconCalendarBtn;
	private SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
	private EmployeeController controller;
	private boolean flag = false;

	private GridBagConstraints cs;

	public EmployeeNewView(JFrame parent, EmployeeController controller) {
		this.controller = controller;
		this.parent = parent;
		parent.setTitle("Novo Funcionario");
		JCFrame = new JFrame("Calendario");
		btnPanel = new JPanel();
		setupLayout();
		setupComponents();
		setupEvents();
		setVisible(true);
		parent.setResizable(true);
	}

	public EmployeeNewView(JFrame parent, Employee employee, EmployeeController controller) {
		flag = true;
		this.controller = controller;
		this.parent = parent;
		parent.setTitle("Funcionario");
		JCFrame = new JFrame("Calendario");
		btnPanel = new JPanel();
		setupLayout();
		setupComponents();
		setupEvents();
		setVisible(true);
		parent.setResizable(true);
	}

	@Override
	public void setupLayout() {

		setLayout(new GridBagLayout());
		btnPanel.setLayout(new FlowLayout());
		parent.setLayout(new BorderLayout());
		cs = new GridBagConstraints();
		JCFrame.setSize(150, 150);
		JCFrame.setVisible(false);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void setupComponents() {

		// sessao formulario
		cs.insets = new Insets(0, 0, 15, 6);
		cs.anchor = GridBagConstraints.WEST;

		lbId = new JLabel("Numero: ");
		cs.gridx = 0;
		cs.gridy = 0;
		cs.gridwidth = 1;
		add(lbId, cs);

		tfId = new JTextField(3);
		tfId.disable();
		cs.gridx = 1;
		cs.gridy = 0;
		cs.gridwidth = 1;
		add(tfId, cs);

		lbAdminssionDate = new JLabel("Data de admissao:* ");
		cs.gridx = 3;
		cs.gridy = 0;
		cs.gridwidth = 1;
		add(lbAdminssionDate, cs);

		try {
			date = new MaskFormatter("##/##/####");
			date.setValidCharacters("1234567890");
			tfAdmissionDate = new JFormattedTextField(date);
			tfAdmissionDate.setColumns(7);

			cs.gridx = 4;
			cs.gridy = 0;
			cs.gridwidth = 1;
			add(tfAdmissionDate, cs);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		iconCalendarBtn = new ImageIcon("/home/andre/workspace/warehouse/img/cal.png");
		btCalendar = new JButton(iconCalendarBtn);
		btCalendar.setPreferredSize(new Dimension(20, 20));
		cs.gridx = 5;
		cs.gridy = 0;
		cs.gridwidth = 1;
		add(btCalendar, cs);

		lbName = new JLabel("Nome:* ");
		cs.gridx = 0;
		cs.gridy = 1;
		cs.gridwidth = 1;
		add(lbName, cs);

		tfName = new JTextField(40);
		cs.gridx = 1;
		cs.gridy = 1;
		cs.gridwidth = 5;
		add(tfName, cs);

		lbAddress = new JLabel("Endereço: ");
		cs.gridx = 0;
		cs.gridy = 2;
		cs.gridwidth = 1;
		add(lbAddress, cs);

		tfAddress = new JTextField(40);
		cs.gridx = 1;
		cs.gridy = 2;
		cs.gridwidth = 5;
		add(tfAddress, cs);

		lbDistrict = new JLabel("Bairro: ");
		cs.gridx = 0;
		cs.gridy = 3;
		cs.gridwidth = 1;
		add(lbDistrict, cs);

		tfDistrict = new JTextField(20);
		cs.gridx = 1;
		cs.gridy = 3;
		cs.gridwidth = 5;
		add(tfDistrict, cs);

		lbComplement = new JLabel("Complem.:");
		cs.gridx = 0;
		cs.gridy = 4;
		cs.gridwidth = 1;
		add(lbComplement, cs);

		tfComplement = new JTextField(20);
		cs.gridx = 1;
		cs.gridy = 4;
		cs.gridwidth = 5;
		add(tfComplement, cs);

		lbTell = new JLabel("Telefone:");
		cs.gridx = 0;
		cs.gridy = 5;
		cs.gridwidth = 1;
		add(lbTell, cs);

		try {
			tell = new MaskFormatter("(##) ##### - ####");
			tell.setValidCharacters("1234567890");
			tfTell = new JFormattedTextField(tell);
			tfTell.setColumns(11);
			cs.gridx = 1;
			cs.gridy = 5;
			cs.gridwidth = 1;
			add(tfTell, cs);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		lbBirth = new JLabel("Data de nascim.:* ");
		cs.gridx = 3;
		cs.gridy = 5;
		cs.gridwidth = 1;
		add(lbBirth, cs);

		try {
			date = new MaskFormatter("##/##/####");
			date.setValidCharacters("1234567890");
			tfBirth = new JFormattedTextField(date);
			tfBirth.setColumns(7);
			cs.gridx = 4;
			cs.gridy = 5;
			cs.gridwidth = 1;
			add(tfBirth, cs);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		btCalendar2 = new JButton(iconCalendarBtn);
		btCalendar2.setPreferredSize(new Dimension(20, 20));
		cs.gridx = 5;
		cs.gridy = 5;
		cs.gridwidth = 1;
		add(btCalendar2, cs);

		lbPosition = new JLabel("Cargo:*");
		cs.gridx = 0;
		cs.gridy = 6;
		cs.gridwidth = 1;
		add(lbPosition, cs);

		jcPosition = new JComboBox<>();
		cs.gridx = 1;
		cs.gridy = 6;
		cs.gridwidth = 5;
		add(jcPosition, cs);

		lbSector = new JLabel("Setor:*");
		cs.gridx = 0;
		cs.gridy = 7;
		cs.gridwidth = 1;
		add(lbSector, cs);

		jcSector = new JComboBox<>();
		cs.gridx = 1;
		cs.gridy = 7;
		cs.gridwidth = 5;
		add(jcSector, cs);

		lbEnterprise = new JLabel("Empresa:*");
		cs.gridx = 0;
		cs.gridy = 8;
		cs.gridwidth = 1;
		add(lbEnterprise, cs);

		jcEnterprise = new JComboBox<>();
		cs.gridx = 1;
		cs.gridy = 8;
		cs.gridwidth = 5;
		add(jcEnterprise, cs);

		lbSituation = new JLabel("Situaçao:*");
		cs.gridx = 0;
		cs.gridy = 9;
		cs.gridwidth = 1;
		add(lbSituation, cs);

		jcSituation = new JComboBox<>();
		cs.gridx = 1;
		cs.gridy = 9;
		cs.gridwidth = 5;
		add(jcSituation, cs);

		cbEnable = new JCheckBox("Acesso ao sistema");
		cs.gridx = 0;
		cs.gridy = 10;
		cs.gridwidth = 2;
		add(cbEnable, cs);

		lbPassword = new JLabel("Senha: ");
		cs.gridx = 0;
		cs.gridy = 11;
		cs.gridwidth = 1;
		add(lbPassword, cs);

		pfPassword = new JPasswordField(20);
		pfPassword.setEnabled(false);
		cs.gridx = 1;
		cs.gridy = 11;
		cs.gridwidth = 2;
		add(pfPassword, cs);

		lbConfirmPass = new JLabel("Confirmar Senha: ");
		cs.gridx = 0;
		cs.gridy = 12;
		cs.gridwidth = 1;
		add(lbConfirmPass, cs);

		pfConfirmPass = new JPasswordField(20);
		pfConfirmPass.setEnabled(false);
		cs.gridx = 1;
		cs.gridy = 12;
		cs.gridwidth = 2;
		add(pfConfirmPass, cs);

		// Preenche os comboboxes

		loadComboBox();

		// sessao do calendario

		jcDate = new JCalendar(locale);
		jcDate2 = new JCalendar(locale);
		btcCancel = new JButton("Cancelar");
		btcOk = new JButton("Confirmar");
		btcOk2 = new JButton("Confirmar");

		// sessao de botoes

		btRegister = new JButton("Criar");
		btCancel = new JButton("Cancelar");

		btnPanel.add(btRegister);
		btnPanel.add(btCancel);

		// sessao para adiçao de PANEL no frame

		parent.add(this, BorderLayout.CENTER);
		parent.add(btnPanel, BorderLayout.SOUTH);

	}

	@Override
	public void setupEvents() {

		btRegister.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				viewValidate();
				//save();
				//parent.dispose();

			}
		});

		cbEnable.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (cbEnable.isSelected()) {
					pfPassword.setEnabled(true);
					pfConfirmPass.setEnabled(true);
				} else {
					pfPassword.setEnabled(false);
					pfConfirmPass.setEnabled(false);
					pfPassword.setText("");
					pfConfirmPass.setText("");
				}

			}
		});

		btCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				parent.dispose();

			}
		});

		// sessao calendario

		btcOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				tfAdmissionDate.setText(formatDate.format(jcDate.getDate()));
				JCFrame.setVisible(false);

			}
		});

		btcOk2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				tfBirth.setText(formatDate.format(jcDate2.getDate()));
				JCFrame.setVisible(false);

			}
		});

		btcCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JCFrame.setVisible(false);
			}
		});

		btCalendar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loadCalendar(jcDate, btcOk);
			}
		});

		btCalendar2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loadCalendar(jcDate2, btcOk2);
			}
		});

	}

	private void loadCalendar(JCalendar calendar, JButton btcOk) {
		JCFrame.removeAll();
		JCFrame = new JFrame("Calendario");
		JCFrame.setLayout(new FlowLayout());
		JCFrame.add(calendar);
		JCFrame.add(btcOk);
		JCFrame.add(btcCancel);
		JCFrame.setResizable(false);
		JCFrame.setSize(250, 200);
		ViewHelpers.center(JCFrame);
		JCFrame.setVisible(true);
	}

	private void loadComboBox() {

		ComboObject cbo = controller.getComboBox();
		for (Enterprise obj : cbo.list1) {
			jcEnterprise.addItem(obj);
		}

		for (Sector obj : cbo.list2) {
			jcSector.addItem(obj);
		}

		for (Situation obj : cbo.list3) {
			jcSituation.addItem(obj);
		}

		for (Position obj : cbo.list4) {
			jcPosition.addItem(obj);
		}
	}

	private void save() {

		Employee employee = new Employee();
		try {
			employee.setName(tfName.getText());
			employee.setAddress(tfAddress.getText());
			employee.setAdmissionDate(formatDate.parse(tfAdmissionDate.getText()));
			employee.setBirthDate(formatDate.parse(tfBirth.getText()));
			employee.setComplement(tfComplement.getText());
			employee.setDistrict(tfDistrict.getText());
			Enterprise ent = (Enterprise) jcEnterprise.getSelectedItem();
			Position pos = (Position) jcPosition.getSelectedItem();
			Situation sit = (Situation) jcSituation.getSelectedItem();
			Sector sec = (Sector) jcSector.getSelectedItem();
			employee.setEnterpriseId(ent.getId());
			employee.setPositionId(pos.getId());
			employee.setSituationId(sit.getId());
			employee.setSectorId(sec.getId());
			employee.setTell(Long.parseLong(ViewHelpers.removeMaks(tfTell.getText())));
		} catch (Exception e) {
			System.out.println("erro ao salvar!! \n erro ao salvar dados no objeto\n" + e);
		}
	}

	private boolean viewValidate() {

		String msg = "";
		boolean flag = true;
		
		if (ViewHelpers.isEmpty(tfName)) {
			tfName.setBackground(Color.pink);
			msg = msg + " *Nome, nao pode ser vazio\n";
			flag = false;

		} else if (!ViewHelpers.validateDate(tfAdmissionDate.getText())) {
			tfName.setBackground(Color.pink);
			msg = msg + " *Data de invalida!!\n";
			flag  = false;

		} else if (!ViewHelpers.validateDate(tfBirth.getText())) {
			tfName.setBackground(Color.pink);
			msg = msg + " *Data invalida!!\n";
			flag = false;
		} if (!flag){
			JOptionPane.showMessageDialog(null,msg, "Erro", JOptionPane.ERROR_MESSAGE);
		}
		return flag;
	}

	public static void main(String[] args) {

		JFrame frame = new JFrame();

		MainController main = new MainController();

		EmployeeNewView view = new EmployeeNewView(frame, main.employeeController);

		frame.setSize(650, 550);

		frame.setVisible(true);

		frame.add(view);

		ViewHelpers.center(frame);

	}
}
