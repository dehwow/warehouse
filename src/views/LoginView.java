package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import controlloers.LoginController;
import helpers.ViewHelpers;

@SuppressWarnings("serial")
public class LoginView extends JPanel implements VisualWindow {

	private JTextField tfUsername;
	private JPasswordField pfPassword;
	private JLabel lbUsername;
	private JLabel lbPassword;
	private JButton btnLogin;
	private JButton btnCancel;
	private JPanel bp;
	private JPanel ic;
	private ImageIcon image;
	private JLabel lbimage;
	private GridBagConstraints cs;
	private LoginController controller;
	private Frame parent;

	public LoginView(Frame parent, LoginController controller) {

		this.controller = controller;
		this.parent = parent;
		parent.setTitle("Login");
		setupLayout();
		setupComponents();
		setupEvents();
		setVisible(true);
		parent.setLayout(new BorderLayout());
		parent.add(this, BorderLayout.CENTER);
		parent.add(bp, BorderLayout.PAGE_END);
		parent.add(ic, BorderLayout.NORTH);
		parent.setResizable(false);
		
	}

	@Override
	public void setupLayout() {

		setLayout(new GridBagLayout());
		cs = new GridBagConstraints();
		bp = new JPanel();
		ic = new JPanel();
		bp.setLayout(new FlowLayout());
		ic.setLayout(new FlowLayout());

	}

	@Override
	public void setupComponents() {

		cs.fill = GridBagConstraints.HORIZONTAL;

		lbUsername = new JLabel("Usuario:* ");
		cs.gridx = 0;
		cs.gridy = 0;
		cs.gridwidth = 1;
		add(lbUsername, cs);

		tfUsername = new JTextField(20);
		cs.gridx = 1;
		cs.gridy = 0;
		cs.gridwidth = 2;
		add(tfUsername, cs);

		lbPassword = new JLabel("Senha:* ");
		cs.gridx = 0;
		cs.gridy = 1;
		cs.gridwidth = 1;
		add(lbPassword, cs);

		pfPassword = new JPasswordField(20);
		cs.gridx = 1;
		cs.gridy = 1;
		cs.gridwidth = 2;
		add(pfPassword, cs);

		image = new ImageIcon("/home/andre/workspace/warehouse/img/11.png");
		lbimage = new JLabel(image);
		setBorder(new LineBorder(Color.gray));
		ic.add(lbimage);

		btnLogin = new JButton("Login");
		btnCancel = new JButton("Cancelar");

		bp.add(btnLogin);
		bp.add(btnCancel);

	}

	@Override
	public void setupEvents() {
		

		tfUsername.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				if (ViewHelpers.isEmpty(tfUsername)) {

					ViewHelpers.errorMessage("esse campo nao pode estar vazio!!");
				}

			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		btnLogin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String username = tfUsername.getText();
				String password = String.valueOf(pfPassword.getPassword());

				try {
					if (controller.checkAutentic(username, password)) {

						parent.dispose();

					} else {

						pfPassword.setText("");

					}
				} catch (HeadlessException | NoSuchAlgorithmException | UnsupportedEncodingException e1) {

					e1.printStackTrace();
				}
			}
		});

		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				System.exit(0);
			}
		});

	}
}
