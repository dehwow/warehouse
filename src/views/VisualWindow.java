package views;

public interface VisualWindow {

	void setupLayout();

	void setupComponents();

	void setupEvents();

}
