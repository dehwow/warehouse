package models;

public class User {
	
	private Integer id;
	private Integer emplooyeeId;
	private String password;
	private Integer permission;
	private String salt;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEmplooyeeId() {
		return emplooyeeId;
	}
	public void setEmplooyeeId(Integer emplooyeeId) {
		this.emplooyeeId = emplooyeeId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getPermission() {
		return permission;
	}
	public void setPermission(Integer permission) {
		this.permission = permission;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String jump) {
		this.salt = jump;
	}

	
}
