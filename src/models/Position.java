package models;

public class Position {

	private int id;
	private String name;
	private double salaryBase;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalaryBase() {
		return salaryBase;
	}
	public void setSalaryBase(double salaryBase) {
		this.salaryBase = salaryBase;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
}
