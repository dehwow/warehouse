package helpers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

import models.User;

public class LoginHelpers {

	private static LoginHelpers loginAdm = null;

	private String getHash(String password, String salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		String alg = "SHA-256";
		MessageDigest encoder = MessageDigest.getInstance(alg);

		encoder.reset();
		encoder.update(this.base64ToByte(salt));

		byte[] hash = encoder.digest(password.getBytes("UTF-8"));

		for (int i = 0; i < 1077; i++) {
			encoder.reset();
			hash = encoder.digest(hash);
		}

		return this.byteToBase64(hash);
	}

	/** Gera um "jump" aleatório de 64 bit. */
	private String getRandomSalt() throws NoSuchAlgorithmException {
		byte[] salt = new byte[16];
		String alg = "SHA1PRNG";
		SecureRandom random = SecureRandom.getInstance(alg);
		random.nextBytes(salt);
		return this.byteToBase64(salt);
	}

	/** @see Decoder#decode(byte[]) */
	private byte[] base64ToByte(String base64) {
		return Base64.getDecoder().decode(base64);
	}

	/** @see Encoder#encodeToString(byte[]) */
	private String byteToBase64(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}
	/** criptografa a senha */
	public void encoder(User user) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		String password = user.getPassword();
		String salt = getRandomSalt();
		password = getHash(password, salt);
		user.setPassword(password);
		user.setSalt(salt);
	}

	/** Faz a autenticaçao. */
	public boolean authenticate(String guestPass, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		

		String salt = user.getSalt();
		String userPassword = user.getPassword();
		guestPass = getHash(guestPass, salt);;


		if (userPassword.equals(guestPass)) {
			return true;
		} else {
			return false;
		}
	}

	public static LoginHelpers getInstance() {
		if (loginAdm == null) {
			loginAdm = new LoginHelpers();
		}
		return loginAdm;
	}

}
