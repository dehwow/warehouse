package helpers;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;


import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ViewHelpers {

	public static void center(Component componente) {
		// Centraliza a janela de abertura no centro do desktop.
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle r = componente.getBounds();

		// Dimensões da janela
		int widthSplash = r.width;
		int heightSplash = r.height;

		// calculo para encontrar as cooredenadas X e Y para a centralização da
		// janela.
		int posX = (screen.width / 2) - (widthSplash / 2);
		int posY = (screen.height / 2) - (heightSplash / 2);

		componente.setBounds(posX, posY, widthSplash, heightSplash);
	}

	public static boolean isEmpty(JTextField component) {
		return component.getText().equals("");
	}

	public static void errorMessage(String message) {
		JOptionPane.showMessageDialog(null, message, "Erro", JOptionPane.ERROR_MESSAGE);
	}

	public static String removeMaks(String str){
	    return str.replaceAll("\\D", "");
	}
	
	public static boolean validateDate(String date) {
		SimpleDateFormat val = new SimpleDateFormat("dd/MM/yyyy");
		val.setLenient(false);
		try {
			val.parse(date);
			return true;
		} catch (Exception e) {
			System.out.println("Data invalida! \n" + e );
			return false;
		}
	}
}
