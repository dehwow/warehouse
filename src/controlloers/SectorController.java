package controlloers;

import java.util.ArrayList;

import DAO.SectorDAO;
import models.Sector;

public class SectorController {

	private MainController controllers;
	private SectorDAO dao = new SectorDAO();

	public SectorController(MainController controllers) {
		this.controllers = controllers;
	}
	
	public ArrayList<Sector> all() {
		return dao.all();
	}

}
