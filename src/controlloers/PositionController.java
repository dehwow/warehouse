package controlloers;

import java.util.ArrayList;

import DAO.PositionDAO;
import models.Position;

public class PositionController {
	
	private MainController controllers;
	private PositionDAO dao = new PositionDAO();
	
	public PositionController(MainController controllers){
		this.controllers = controllers;
	}
	
	public ArrayList<Position> all(){
		return dao.all();
	}

}
