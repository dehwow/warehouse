package controlloers;

import helpers.ComboObject;
import models.Employee;
import views.EmployeeNewView;

public class EmployeeController {

	private MainController controllers;

	public EmployeeController(MainController controllers) {
		this.controllers = controllers;
	}

	public void create() {

	}

	public void save(Employee employee) {
	}

	public void edit(int id) {
	}

	public void delete(int id) {
	}

	public void show() {
	}
	
	public ComboObject getComboBox(){		
		
		ComboObject cbo = new ComboObject();
		cbo.list1 = controllers.enterpriseController.all();
		cbo.list2 = controllers.sectorController.all();
		cbo.list3 = controllers.situationController.all();
		cbo.list4 = controllers.positionController.all();
		return cbo;

	}
	
	public static void main(String[] args) {
		
	}
	
}