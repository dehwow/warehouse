package controlloers;

import models.User;

public class MainController {

	public EmployeeController employeeController;
	protected ItemController itemController;
	protected OrderController orderController;
	protected User currentUser;
	protected LoginController loginController;
	protected EnterpriseController enterpriseController;
	protected SectorController sectorController;
	protected PositionController positionController;
	protected SituationController situationController;

	public MainController() {

		employeeController = new EmployeeController(this);
		itemController = new ItemController(this);
		orderController = new OrderController(this);
		loginController = new LoginController(this);
		enterpriseController = new EnterpriseController(this);
		sectorController = new SectorController(this);
		positionController = new PositionController(this);
		situationController = new SituationController(this);
		
		
	}

	public static void start() {
		MainController main = new MainController();
		main.loginController.show();
		main.employeeController.create();
	}

	public static void main(String[] args) {
		MainController.start();
	}
}
