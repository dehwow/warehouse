package controlloers;

import java.util.ArrayList;

import DAO.EnterpriseDAO;
import models.Enterprise;

public class EnterpriseController {

	private MainController controllers;
	private EnterpriseDAO dao = new EnterpriseDAO();
	
	public EnterpriseController(MainController controllers){
		this.controllers = controllers;
	}
	
	public ArrayList<Enterprise> all(){
		return dao.all();
	}
}
