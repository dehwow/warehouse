package controlloers;

import java.awt.HeadlessException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.swing.JFrame;


import DAO.UserDAO;
import helpers.LoginHelpers;
import helpers.ViewHelpers;
import models.User;
import views.LoginView;

public class LoginController {

	@SuppressWarnings("unused")
	private MainController controllers;
	@SuppressWarnings("unused")
	private LoginView loginView;
	private UserDAO userDAO;
	private LoginHelpers loginAdm;

	public LoginController(MainController controllers) {
		
		this.controllers = controllers;
		userDAO = new UserDAO();
		loginAdm = LoginHelpers.getInstance();

	}

	public void show() {
		
		JFrame frame = new JFrame();
		frame.setSize(300, 200);
		ViewHelpers.center(frame);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginView = new LoginView(frame, this);
	}

	public boolean checkAutentic(String username, String password)
			throws HeadlessException, NoSuchAlgorithmException, UnsupportedEncodingException {
		
		User currentUser = userDAO.findUser(username);

		if (currentUser == null) {

			ViewHelpers.errorMessage("Usuario ou senha invalidos!!");
			return false;

		} else if (!loginAdm.authenticate(password, currentUser)) {

			ViewHelpers.errorMessage("Usuario ou senha invalidos");
			return false;
		}

		return true;
	}

}
