package controlloers;

import java.util.ArrayList;

import DAO.SituationDAO;
import models.Situation;

public class SituationController {

	private MainController controllers;
	private SituationDAO dao = new SituationDAO();
	
	public SituationController(MainController controllers) {
		this.controllers = controllers;
	}
	
	public ArrayList<Situation> all() {
		return dao.all();
	}
}
